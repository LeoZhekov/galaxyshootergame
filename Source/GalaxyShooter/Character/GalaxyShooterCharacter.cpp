// Fill out your copyright notice in the Description page of Project Settings.


#include "GalaxyShooterCharacter.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Turret/Turret.h"

// Sets default values
AGalaxyShooterCharacter::AGalaxyShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGalaxyShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
  if (!ensure(TurretBlueprint)) { return; }
  Turret = GetWorld()->SpawnActor<ATurret>(TurretBlueprint);
  auto Child = GetCapsuleComponent()->GetChildComponent(1);
  auto Socket = Child->GetAllSocketNames()[0];
  Turret->AttachToComponent(Child, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), Socket);
  InputComponent->BindAction("Fire", IE_Pressed, Turret, &ATurret::OnFire);
}

// Called every frame
void AGalaxyShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGalaxyShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

  // Bind movement events
  InputComponent->BindAxis("MoveUp", this, &AGalaxyShooterCharacter::MoveUp);
  InputComponent->BindAxis("MoveRight", this, &AGalaxyShooterCharacter::MoveRight);

}

void AGalaxyShooterCharacter::MoveUp(float Value) {
  if (Value != 0.0f) {
    AddMovementInput(GetActorUpVector(), Value);
  }
}

void AGalaxyShooterCharacter::MoveRight(float Value) {
  if (Value != 0.0f) {
    AddMovementInput(GetActorRightVector(), Value);
  }
}
