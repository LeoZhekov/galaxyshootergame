// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "GalaxyShooterCharacter.generated.h"

UCLASS()
class GALAXYSHOOTER_API AGalaxyShooterCharacter : public APaperCharacter
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<class ATurret> TurretBlueprint;

public:
	// Sets default values for this character's properties
	AGalaxyShooterCharacter();

protected:
	// Called when the game starts or when spawned

	virtual void BeginPlay() override;
	/** Handles moving forward/backward */
	void MoveUp(float Value);

	/** Handles stafing movement, left and right */
	void MoveRight(float Value);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


private:
	class ATurret* Turret;
};
