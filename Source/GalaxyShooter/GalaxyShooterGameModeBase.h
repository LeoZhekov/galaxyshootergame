// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GalaxyShooterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GALAXYSHOOTER_API AGalaxyShooterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
