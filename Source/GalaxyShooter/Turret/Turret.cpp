// Fill out your copyright notice in the Description page of Project Settings.


#include "Turret.h"
#include "Projectiles\ProjectileLevelOne.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATurret::ATurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FP_TurretRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	FP_TurretRoot->AttachTo(RootComponent);
	SetRootComponent(FP_TurretRoot);
}

// Called when the game starts or when spawned
void ATurret::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ATurret::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{

			const FRotator SpawnRotation = FP_TurretRoot->GetComponentRotation();
			const FVector SpawnLocation = FP_TurretRoot->GetComponentLocation();

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			// spawn the projectile at the muzzle
			World->SpawnActor<AProjectileLevelOne>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Projectile"))
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No FireSound"))
	}

}
